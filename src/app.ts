import express from 'express'
const app = express()

app.get('/', (req, res) => {
  res.send(`this ${process.env.APP} app run properly`)
})

app.get('/call/:name', (req, res) => {
  res.json({ message: `hello ${req.params.name}!` })
})

app.get('/cool', (req, res) => {
  res.send('Coolllll')
})

export default app